/*! \file kmeans_tree.h
 * \brief kmeans tree functions
 */
#ifndef K_MEANS_TREE_H
#define K_MEANS_TREE_H

#include <vector>
#include <cstdlib>
#include <mutex>

namespace kmtree
{

///
/// \brief The virtual class of tree node
///
class TreeNode
{
public:
	TreeNode():des(nullptr)  {}
	virtual ~TreeNode()
	{
		if(des != nullptr)
		{
			delete [] des;
			des = nullptr;
		}
	}
	// pure virtual function that will be implemented in derived classes
	virtual bool RecursiveBuild(size_t num_keys, int dim, int depth, int depth_curr, int bf, float **p , double *means, int *assign, int thread_num = 1) = 0;
	virtual bool ClearNode(int bf) = 0;
	virtual size_t CountNodes(int branch_num) const = 0;
	virtual size_t CountLeaves(int branch_num) const = 0;
	virtual bool Compare(TreeNode *in, int branch_num, int dim) const = 0;
	virtual void DescendFeature(float *q, float *v, int branch_num, int dim) = 0;
	
	float *des; //!< the descriptor vector
	size_t num_points = 0;
	float** points = nullptr;
};

///
/// \brief The interior node class of the tree
///
class TreeInNode: public TreeNode
{
public:
	TreeInNode():children(nullptr) {}
	virtual ~TreeInNode();
	virtual bool RecursiveBuild(size_t num_keys, int dim, int depth, int depth_curr, int bf, float **p, double *means, int *assign, int thread_num = 1);
	virtual bool ClearNode(int bf);
	virtual size_t CountNodes(int branch_num) const ;
	virtual size_t CountLeaves(int branch_num) const ;
	virtual bool Compare(TreeNode *in, int branch_num, int dim) const;
	virtual void DescendFeature(float *q, float *v, int branch_num, int dim);
	
	TreeNode **children = nullptr;
};

///
/// \brief The leaf node class of the tree
///
class TreeLeafNode: public TreeNode
{
public:
	TreeLeafNode() {}
	virtual bool RecursiveBuild(size_t num_keys, int dim, int depth, int depth_curr, int bf, float **p , double *means, int *assign, int thread_num = 1);
	virtual bool ClearNode(int bf);
	virtual size_t CountNodes(int branch_num) const;
	virtual size_t CountLeaves(int branch_num) const;
	virtual bool Compare(TreeNode *leaf, int branch_num, int dim) const;
	virtual void DescendFeature(float *q,float *v,  int branch_num, int dim);
};

///
/// \brief The kmeans tree class. The depth of the root is 0. The depth of the leaf nodes is depth+1.
///
class KMeansTree
{
public:
	
	KMeansTree();
	KMeansTree(int depth_, int branch_num_, int dim_); //!< constructor where define depth, branch num and dimension of data
	~KMeansTree();

	bool BuildTree(size_t num_keys, float** p, int thread_num = 1);      //!< build a vocabulary tree from a set of features
	void ANN(float* q, float* v, int branch_num, int dim); //!< find approximate nearest neighbor
	bool ClearTree();                   //!< release the memory
	bool Compare(KMeansTree &v) const; //!< compare two vocabulary tree and returns whether they are the same
	void Show() const;      //!< a test function
	
	int branch_num = 0;             //!< the branch number of a node
	int depth = 0;                  //!< the depth of the tree
	int dim = 0;                    //!< the dimension of the descriptor
 	size_t num_nodes = 0;           //!< the number of nodes in the tree
	size_t num_leaves = 0;          //!< the number of leaf nodes in the tree
	TreeNode *root = nullptr;       //!< the root of the tree
};

}   // end of namespace kmtree

#endif  // K_MEANS_TREE_H
