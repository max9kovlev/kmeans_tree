cmake_minimum_required(VERSION 3.16)

project(kmeans_tree)

add_executable(kmeans_tree kmeans_tree_test.cpp kmeans_tree.h kmeans_tree.cpp clustering.h clustering.cpp global_params.h global_params.cpp)
target_link_libraries(kmeans_tree -fopenmp)
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/data.txt
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR})