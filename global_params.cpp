/*! \file global_params.cpp
 * \brief global parameters and utility functions implementation
 *
 * This file contains some global parameters shared by the whole project, such as verbose, etc.
 */
#include <iostream>
#include <string>

#include "global_params.h"

using std::cout;
using std::endl;

namespace kmtree{

int GlobalParam::Verbose = 1;

}	// end of namespace kmtree
