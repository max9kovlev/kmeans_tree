/*! \file clustering.h
 * \brief clustering algorithms, such as k-means
 */
#ifndef KMTREE_CLUSTERING_H
#define KMTREE_CLUSTERING_H

#include "global_params.h"

namespace kmtree
{
	double Kmeans(size_t num, int dim, int k, float **p, double *means, int *assignment, int thread_num);
}	// end of namespace kmtree

#endif	//KMTREE_CLUSTERING_H
