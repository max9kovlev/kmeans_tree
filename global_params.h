/*! \file global_params.h
 * \brief global parameters and utility functions
 *
 * This file contains some global parameters shared by the whole project, such as verbose etc.
 */

#ifndef KMTREE_GLOBAL_PARAMS_H
#define KMTREE_GLOBAL_PARAMS_H

namespace kmtree{
/**
 * @brief global parameters
 */
class GlobalParam
{
    public:
        static int Verbose;
};

}	// end of namespace kmtree
#endif  //VOT_GLOBAL_PARAMS_H
