﻿#include <iostream>
#include "kmeans_tree.h"
#include <string>
#include <sstream>
#include <fstream>
#include "global_params.h"

inline float l2sq(const float* a, const float* b, const int dim)
{
	float sum(0);
	for (size_t i = 0; i < dim; ++i) {
		sum += (a[i] - b[i]) * (a[i] - b[i]);
	}

	return sqrt(sum);
}

void PrintPoint(float* p, int dim) {
	for (int i(0); i < dim; ++i) {
		std::cout << p[i] << " ";
	}
	std::cout << "\n";
}

int main()
{
	kmtree::KMeansTree tree = kmtree::KMeansTree(8, 5, 2); // (int depth_, int branch_num_, int dim_)
	std::vector<std::vector<int>> data(0);
	std::ifstream inFile;
	const std::string PATH = "a1.txt";

	// Reading dataset
	inFile.open(PATH);
	std::string s;
	std::size_t i = 0;
	while (std::getline(inFile, s)) {
		std::vector<int> vec(2);
		std::istringstream line_stream(s);
		for (auto& x : vec) {
			line_stream >> x;
		}
		data.push_back(vec);
		i++;
	}
	kmtree::GlobalParam::Verbose = false;
	std::cout << "End of reading dataset\n";
	std::cout << "Size of train_data " << data.size() - 500 << "\n";
	inFile.close();

	float** mem_pointer = new float * [data.size()];
	for (int i(0); i < data.size(); ++i) {
		mem_pointer[i] = new float[2];
		for (int j(0); j < data[i].size(); ++j) {
			mem_pointer[i][j] = data[i][j];
		}
	}
	

	tree.BuildTree(data.size(), mem_pointer); // (size_t num_keys,  float **p) 
	/*float* query = new float[20];
	
	tree.ANN(query, mem_pointer[9000], 5, 20);
	std::cout << "Query input point: \n";
	PrintPoint(mem_pointer[9000], 20);
	
    std::cout << "Query output point\n";
	PrintPoint(query, 20);

	*/
	/*float min_dist = std::numeric_limits<float>::max();
	float dist;
	float score = 0;
	float* query = new float[2];
	int best_idx;
	for (int i(2500); i < 3000; i++) {
		for (int j(0); j < 2500; j++) {
			dist = l2sq(mem_pointer[i], mem_pointer[j], 2);
			if (dist < min_dist) {
				best_idx = j;
				min_dist = dist;
			}
		}
		tree.ANN(query, mem_pointer[i],5, 2);
		if (query[0] == mem_pointer[best_idx][0] && query[1] == mem_pointer[best_idx][1])
			score++;
		PrintPoint(mem_pointer[best_idx], 2);
		PrintPoint(query, 2);
		std::cout << "-----------------------------------------\n";
	}
	std::cout << "Total score: " << score / 500.0 << "\n";*/
	float score = 0;
	float* query = new float[2];
	for (int i = 0; i < data.size(); i++) {
		tree.ANN(query, mem_pointer[i], 5, 2);
		//PrintPoint(mem_pointer[i], 2);
		//PrintPoint(query, 2);
		//std::cout << "------------\n";
		if (query[0] == mem_pointer[i][0] && query[1] == mem_pointer[i][1])
			score++;
	}
	std::cout << score / 3000.0 << "\n";
	//tree.ANN(query, mem_pointer[2700], 5, 2);
	//PrintPoint(mem_pointer[2700], 2);
	//PrintPoint(query, 2);
	tree.Show();
	tree.ClearTree();
	return 0;
}
