/*! \file kmeans_tree.cpp
 * \brief kmeans tree functions implementations
 */
#include <iostream>
#include <cmath>
#include <cassert>
#include <limits>
#include <thread>
#include <algorithm>
#include <numeric>
#include <functional>
#include "global_params.h"
#include "kmeans_tree.h"
#include "clustering.h"


using std::cout;
using std::endl;

inline float l2sq(const float *a, const float *b, const int dim)
{
	float sum(0);
	for (size_t i = 0; i < dim; ++i){
		sum += (a[i] - b[i]) * (a[i] - b[i]);
	}
	
	return sqrt(sum);
}

namespace kmtree
{
/** KMeansTree Class Implementation */
KMeansTree::KMeansTree() {}

KMeansTree::KMeansTree(int depth_, int branch_num_, int dim_):
    branch_num(branch_num_), depth(depth_), dim(dim_) {};

KMeansTree::~KMeansTree() {root = nullptr;}  // do nothing since root is undetermined

TreeInNode::~TreeInNode()
{
	if (children != nullptr) {
		delete [] children;
		children = nullptr;
	}
}

//ANN FOR ONE POINT
void KMeansTree::ANN(float* q, float* v, int branch_num, int dim)
{
	root->DescendFeature(q, v, branch_num, dim);
}


bool KMeansTree::ClearTree()
{
	if (root != nullptr) {
		root->ClearNode(branch_num);
	}

	std::cout << "[KMeansTree] Successfully clearing the tree\n";
	return true;
}

bool TreeInNode::ClearNode(int bf)
{
	for (int i = 0; i < bf; i++) {
		if (children[i] != nullptr)
			children[i]->ClearNode(bf);
	}
	delete this;
	return true;
}

bool TreeLeafNode::ClearNode(int bf)
{
	delete this;
	return true;
}




bool KMeansTree::BuildTree(size_t num_keys, float **p, int thread_num)
{
	double *means = new double [branch_num * dim];
	int *assign = new int [num_keys];
	if (means == nullptr || assign == nullptr) {
		std::cout << "[KMeansTree Build] Error allocating memory in K-means\n";
		return false;
	}

	root = new TreeInNode();
	root->des = new float [dim];
	for (int i = 0; i < dim; i++)
		root->des[i] = 0;

	if (!root->RecursiveBuild(num_keys, dim, depth, 0, branch_num, p, means, assign, thread_num))
		return false;

	delete [] means;
	delete [] assign;

	std::cout << "[KMeansTree Build] Finish building kmeans tree!\n";
	num_nodes = root->CountNodes(branch_num);
	num_leaves = root->CountLeaves(branch_num);
	return true;
}

void MultiRecursiveBuild(TreeNode *children, size_t num_keys, int dim, int depth, int depth_curr, int bf, float **p, double *means, int *assign, int sub_thread_num)
{
	if (children != nullptr) {
		children->RecursiveBuild(num_keys, dim, depth, depth_curr, bf, p, means, assign, sub_thread_num);
	}
}

bool TreeInNode::RecursiveBuild(size_t num_keys, int dim, int depth, int depth_curr, int bf, float **p, double *means, int *assign, int thread_num)
{
	if (GlobalParam::Verbose && depth_curr < 3)
		std::cout << "[RecursiveBuild] K-means in depth " << depth_curr << "\n";

	double error = Kmeans(num_keys, dim, bf, p, means, assign, thread_num);
	if (std::abs(error + 1) < 10e-6) {
		std::cerr << "[Error] Error in TreeInNode::RecursiveBuild\n";
		return false;
	}

	// the average distance between each cluster and the node descriptor
	double mean_distance = 0.0;
	for (int i = 0; i < bf; i++) {
		for (int j = 0; j < dim; j++) {
			mean_distance += (means[i*dim + j] - des[j]) * (means[i*dim + j] - des[j]);
		}
	}
	mean_distance /= bf;
	if (GlobalParam::Verbose && depth_curr < 3)
		std::cout << "[RecursiveBuild] Group/Center error: " << error/num_keys << "/" << mean_distance << "\n";

	children = new TreeNode* [bf];
	size_t *counts = new size_t [bf];
	for (int i = 0; i < bf; i++)
		counts[i] = 0;
	for (int i = 0; i < num_keys; i++)
		counts[assign[i]]++;

	for (int i = 0; i < bf; i++) {
		if (counts[i] > 0) {   		
			if (depth_curr == depth || counts[i] <= bf) 
			{
				children[i] = new TreeLeafNode();
				children[i]->points = new  float* [counts[i]];
				children[i]->num_points = counts[i];
				int k = 0;
				for (int j = 0; j < num_keys; j++) {
					if (assign[j] == i) {
						children[i]->points[k] = p[j];
						k++;
					}
				}
			}
			else
				children[i] = new TreeInNode();
			children[i]->des = new float [dim];
			for (int j = 0; j < dim; j++) {
				if (sizeof(float) == 1)          // (char) round to the nearest integer
					children[i]->des[j] = (float)(means[i*dim + j] + 0.5);
				else                            // (float)
					children[i]->des[j] = (float)means[i*dim + j];
			}
		}
		else {
			children[i] = nullptr;
		}
	}

	// rearrange the pointer array so that after rearrangement, the sift keys in the first cluster appear consecutively in the array first,
	// then the second cluster, so on and so forth
	size_t idx = 0;
	size_t start_idx = 0;
	for (size_t i = 0; i < bf; i++) {
		for (size_t j = start_idx; j < num_keys; j++) {
			if (assign[j] == i) {
				// swap the pointer
				float *temp = p[idx];
				p[idx] = p[j];
				p[j] = temp;
				// swap the assignment
				unsigned int temp_assign = assign[idx];
				assign[idx] = assign[j];
				assign[j] = temp_assign;

				idx++;
			}
		}
		start_idx += counts[i];
	}
	assert(start_idx == num_keys);

	// recursively build the tree in the children nodes
	if (thread_num == 1 || thread_num < bf) {     // single-thread
		size_t offset = 0;
		for (int i = 0; i < bf; i++) {
			if (children[i] != nullptr) {
				children[i]->RecursiveBuild(counts[i], dim, depth, depth_curr+1, bf, p+offset, means, assign+offset, thread_num);
			}
			offset += counts[i];
		}
	}
	else {       // multi-thread
		std::vector<std::thread> threads;
		int subtree_threads = thread_num / bf;
		size_t offset = 0;
		std::vector<double *>sub_means(bf);
		for (int i = 0; i < bf; i++) {
			int sub_thread_num = 0;
			if (i == bf - 1) {
				sub_thread_num = thread_num - (bf - 1) * subtree_threads;
			}
			else {
				sub_thread_num = subtree_threads;
			}
			sub_means[i] = new double [bf * dim];
			std::copy(means, means + bf * dim, sub_means[i]);
			threads.push_back(std::thread(MultiRecursiveBuild, children[i], counts[i], dim, depth, depth_curr+1, bf, p+offset, sub_means[i], assign+offset, sub_thread_num));

			offset += counts[i];
		}
		std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
		for (int i = 0; i < bf; i++) {
			delete [] sub_means[i];
		}
	}
	
	delete [] counts;
	return true;
}

bool TreeLeafNode::RecursiveBuild(size_t num_keys, int dim, int depth, int depth_curr, int bf, float **p, double *means, int *assign, int thread_num)
{
	return true;
}



void KMeansTree::Show() const
{
	std::cout << "[KMeansTree] depth/branch_num: " << depth << "/" << branch_num << '\n';
	std::cout << "[KMeansTree] #nodes " << num_nodes << '\n';
	std::cout << "[KMeansTree] #leaves " << num_leaves << '\n';
}

size_t TreeInNode::CountNodes(int branch_num) const
{
	size_t num_nodes = 0;
	for (int i = 0; i < branch_num; i++) {
		if (children[i] != nullptr)
			num_nodes += children[i]->CountNodes(branch_num);
	}
	return num_nodes + 1;
}

size_t TreeLeafNode::CountNodes(int branch_num) const {return 1;}

size_t TreeInNode::CountLeaves(int branch_num) const
{
	size_t num_leaves = 0;
	for (int i = 0; i < branch_num; i++) {
		if (children[i] != nullptr)
			num_leaves += children[i]->CountLeaves(branch_num);
	}
	return num_leaves;
}

size_t TreeLeafNode::CountLeaves(int branch_num) const {return 1;}

bool KMeansTree::Compare(KMeansTree & v) const
{
	if (v.dim != dim || v.depth != depth || v.branch_num != branch_num) {
		return false;
	}

	size_t node_count = root->CountNodes(branch_num);
	size_t other_node_count = v.root->CountNodes(branch_num);
	if (node_count != other_node_count) {
		return false;
	}

	size_t leave_count = root->CountLeaves(branch_num);
	size_t other_leave_count = v.root->CountLeaves(branch_num);
	if (leave_count != other_leave_count) {
		return false;
	}

	return root->Compare(v.root, branch_num, dim);
}

bool TreeInNode::Compare(TreeNode *in, int branch_num, int dim) const
{
	TreeInNode *other_node = dynamic_cast<TreeInNode*>(in);
	for (int i = 0; i < dim; i++)
	{
		if (des[i] != other_node->des[i])
			return false;
	}

	for (int i = 0; i < branch_num; i++) {
		if (children[i] == nullptr) {
			if (other_node->children[i] != nullptr)
				return false;
		}
		else {
			if (other_node->children[i] == nullptr)
				return false;
			children[i]->Compare(other_node->children[i], branch_num, dim);
		}
	}
	return true;
}

bool TreeLeafNode::Compare(TreeNode *leaf, int branch_num, int dim) const
{
	TreeLeafNode *other_leaf = dynamic_cast<TreeLeafNode*>(leaf);
	for (int i = 0; i < dim; i++) {
		if (des[i] != other_leaf->des[i])
			return false;
	}
	return true;
}


void TreeInNode::DescendFeature(float *q, float *v, int branch_num, int dim)
{
	int best_idx = 0;
	float min_distance = std::numeric_limits<float>::max();
	for (int i = 0; i < branch_num; i++) {
		if (children[i] != nullptr) {
			float curr_dist = l2sq(v, children[i]->des, dim);
			if (curr_dist < min_distance) {
				min_distance = curr_dist;
				best_idx = i;
			}
		}
	}

	children[best_idx]->DescendFeature(q, v, branch_num, dim);
}

void TreeLeafNode::DescendFeature(float *q,float *v, int branch_num, int dim)
{
	float distance = std::numeric_limits<float>::max();
	int idx = 0;
	for (int i = 0; i < num_points; i++) {
		if (l2sq(points[i], v, dim) < distance) {
			idx = i;
			distance = l2sq(points[i], v, dim);
		}
	}
	std::copy(points[idx], points[idx] + dim, q);
}

size_t leaves_count = 0;    // global leaf counter

}   // end of namespace kmtree
